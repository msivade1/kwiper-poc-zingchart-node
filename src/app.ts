import {zingchart, ZC} from 'zingchart/server/zingchart-nodejs.min.js';
// let { zingchart, ZC } = require('zingchart/server/zingchart-nodejs.min.js');
// or
//let { zingchart } = require('./server/zingchart-nodejs.min.js'); // this also works since recent versions look for license into zingchart.BUILDCODE in addition of ZC.BUILDCODE

zingchart.DEV.CANVASVERSION = 2; // indicates which canvas version is installed so that ZingChart will use the appropriate canvas API

let chartData = {
    graphset:[
        {
            type: 'sunburst',
            title:{
                text:'Sunburst Chart: ZingChart'
            },
            options:{
                slice:0,
                space:0,
                palette: ['white', '#4CA9B7', '#7B6569', '#D8B54B', '#71A436'],

            },
            plot:{
                bandWidths: "10%",
                valueBox: {
                    text:"%plot-text \n %v",
                    color:"white",
                    "fontSize":"20px",
                    connector: {
                        alpha: 1,
                        lineWidth: 2
                    },
                    maxChars: 50, // removes truncates
                    rules: [
                        {
                            rule: '"%plot-text" == "root"',
                            text: ""
                        },
                        // sortir les labels si on a une tranche trop petite
                        {
                            rule: '%node-percent-value < 2',
                            placement: 'out',
                            color: 'black',
                            angle: 0
                        },
                        {
                            rule: '%node-percent-value > 2',
                            placement: 'in',
                            color: 'white',
                        },
                        // ne pas afficher les tranches à 0
                        {
                            rule: '%v === 0',
                            visible: 'false'
                        }
                    ]
                },
                rules: [
                    {
                        rule: '"%data-type" == "US" && "%data-parent" == "immobilier"',
                        backgroundColor: '#9B8589'
                    },
                    {
                        rule: '"%data-type" == "US" && "%data-parent" == "entreprise"',
                        backgroundColor: '#81C4CD'
                    },
                    {
                        rule: '"%data-type" == "US" && "%data-parent" == "financier"',
                        backgroundColor: '#F9D999'
                    },
                    {
                        rule: '"%data-type" == "US" && "%data-parent" == "autres"',
                        backgroundColor: '#427358'
                    },
                    {
                        rule: '"%data-type" == "PP" && "%data-parent" == "immobilier" && "%data-holder" == "client"',
                        backgroundColor: '#9F95A1'
                    },
                    {
                        rule: '"%data-type" == "PP" && "%data-parent" == "immobilier" && "%data-holder" == "conjoint"',
                        backgroundColor: '#807482'
                    },
                    {
                        rule: '"%data-type" == "PP" && "%data-parent" == "entreprise" && "%data-holder" == "client"',
                        backgroundColor: '#96C5D6'
                    },
                    {
                        rule: '"%data-type" == "PP" && "%data-parent" == "entreprise" && "%data-holder" == "conjoint"',
                        backgroundColor: '#64C9CE'
                    },
                    {
                        rule: '"%data-type" == "PP" && "%data-parent" == "financier" && "%data-holder" == "client"',
                        backgroundColor: '#E5AC00'
                    },
                    {
                        rule: '"%data-type" == "PP" && "%data-parent" == "financier" && "%data-holder" == "conjoint"',
                        backgroundColor: '#BF8F00'
                    },
                    {
                        rule: '"%data-type" == "PP" && "%data-parent" == "autres" && "%data-holder" == "client"',
                        backgroundColor: '#548235'
                    },
                    {
                        rule: '"%data-type" == "PP" && "%data-parent" == "autres" && "%data-holder" == "conjoint"',
                        backgroundColor: '#427358'
                    },
                    {
                        rule: '"%data-type" == "NP" && "%data-parent" == "immobilier" && "%data-holder" == "client"',
                        backgroundColor: '#9F95A1',
                        backgroundImage: "PATTERN_BACKWARD_DIAGONAL"
                    },
                    {
                        rule: '"%data-type" == "NP" && "%data-parent" == "immobilier" && "%data-holder" == "conjoint"',
                        backgroundColor: '#807482',
                        backgroundImage: "PATTERN_BACKWARD_DIAGONAL"
                    },
                    {
                        rule: '"%data-type" == "NP" && "%data-parent" == "entreprise" && "%data-holder" == "client"',
                        backgroundColor: '#96C5D6',
                        backgroundImage: "PATTERN_BACKWARD_DIAGONAL"
                    },
                    {
                        rule: '"%data-type" == "NP" && "%data-parent" == "entreprise" && "%data-holder" == "conjoint"',
                        backgroundColor: '#64C9CE',
                        backgroundImage: "PATTERN_BACKWARD_DIAGONAL"
                    },
                    {
                        rule: '"%data-type" == "NP" && "%data-parent" == "financier" && "%data-holder" == "client"',
                        backgroundColor: '#E5AC00',
                        backgroundImage: "PATTERN_BACKWARD_DIAGONAL"
                    },
                    {
                        rule: '"%data-type" == "NP" && "%data-parent" == "financier" && "%data-holder" == "conjoint"',
                        backgroundColor: '#BF8F00',
                        backgroundImage: "PATTERN_BACKWARD_DIAGONAL"
                    },
                    {
                        rule: '"%data-type" == "NP" && "%data-parent" == "autres" && "%data-holder" == "client"',
                        backgroundColor: '#548235',
                        backgroundImage: "PATTERN_BACKWARD_DIAGONAL"
                    },
                    {
                        rule: '"%data-type" == "NP" && "%data-parent" == "autres" && "%data-holder" == "conjoint"',
                        backgroundColor: '#427358',
                        backgroundImage: "PATTERN_BACKWARD_DIAGONAL"
                    }
                ]
            },
            series:[
                {id: 'root', text: '', parent: '', value: 1},
                {id: 'entreprise', text: 'Entreprise', parent: 'root', value: 6000,
                    backgroundImage: "PATTERN_BACKWARD_DIAGONAL"},
                {id: 'immobilier', text: 'Immobilier', parent: 'root', value: 50000},
                {id: 'financier', text: 'Financier', parent: 'root', value: 50000},
                {id: 'autres', text: 'Autres', parent: 'root', value: 50000},
                {id: 'e_alain_pp', text: 'Alain PP', parent: 'entreprise', value: 3000, "data-type": 'PP', "data-parent": 'entreprise', "data-holder": 'client'},
                {id: 'e_veronique_pp', text: 'Veronique PP', parent: 'entreprise', value: 3000, "data-type": 'PP', "data-parent": 'entreprise', "data-holder": 'conjoint'},
                {id: 'i_alain_pp', text: 'Alain PP', parent: 'immobilier', value: 10000, "data-type": 'PP', "data-parent": 'immobilier', "data-holder": 'client'},
                {id: 'i_veronique_np', text: 'Veronique NP', parent: 'immobilier', value: 20000, "data-type": 'NP', "data-parent": 'immobilier', "data-holder": 'conjoint'},
                {id: 'i_alain_us', text: 'Alain US', parent: 'immobilier', value: 20000, backgroundImage: "PATTERN_BACKWARD_DIAGONAL",
                 "data-type": 'US', "data-parent": 'immobilier', "data-holder": 'client'},
                {id: 'f_alain_pp', text: 'Alain PP', parent: 'financier', value: 10000, "data-type": 'PP', "data-parent": 'financier', "data-holder": 'client'},
                {id: 'f_veronique_np', text: 'Veronique NP', parent: 'financier', value: 20000, "data-type": 'NP', "data-parent": 'financier', "data-holder": 'conjoint'},
                {id: 'f_alain_us', text: 'Alain US', parent: 'financier', value: 20000, "data-type": 'US', "data-parent": 'financier', "data-holder": 'client'},
                {id: 'a_alain_pp', text: 'Alain PP', parent: 'autres', value: 25000, "data-type": 'PP', "data-parent": 'autres', "data-holder": 'client'},
                {id: 'a_veronique_pp', text: 'Veronique PP', parent: 'autres', value: 25000, "data-type": 'PP',"data-parent": 'autres', "data-holder": 'conjoint'},
                {id: 'a_veronique_pp_2', text: 'Veronique PP 2', parent: 'a_veronique_pp', value: 2500, "data-type": 'PP',"data-parent": 'autres', "data-holder": 'conjoint'}
            ]
        }
    ]
};
zingchart.MODULESDIR="zingchart/server/modules-nodejs/";
zingchart.loadModules("patterns,sunburst", function(){
    //Render your charts with the modules.
    zingchart.render({
        id : 'zc',
        width : 1500,
        height : 1000,

        data : chartData,
        //dataurl : "http://url.path.to/chart.json",
        filetype : 'png', // png or jpeg
        filename : 'out.png'
    });
});
